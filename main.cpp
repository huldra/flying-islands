// Copyright (c) 2022 Huldra


#include "engine/easy.h"
#include "engine/transform2f.h"
#include "engine/transform3f.h"

using namespace arctic;  // NOLINT


Font g_font;
Vec2Si32 g_wind_map_size(1920,1080);
Si32 g_wind_map_zoom = 1;
Vec2F g_view_pos(0,0);
Vec2F g_player_pos(240,235);
Vec2F g_player_vel(0, 0);
Vec2F g_player_acc(0, 0);
float g_player_volume = 0.5f;
std::vector<Vec2F> *g_wind_map;
std::vector<Vec2F> g_wind_maps[3];
std::vector<Vec2F> g_dust;
Sprite g_sky;
Sprite g_island_1;
Sprite g_island_2;

static void DustRespawn(arctic::Vec2F &p) {
  bool is_ok = false;
  while (!is_ok) {
    Vec2F good_n(0, 0);
    Si64 rnd = Random(0, 1);
    if (Random(0, 1) == 0) {
      good_n.y = 1.f -2.f * rnd;
      p.y = float(rnd * (g_wind_map_size.y - 1));
      p.x = float(Random(0, g_wind_map_size.x - 1));
    } else {
      good_n.x = 1.f -2.f * rnd;
      p.x = float(rnd * (g_wind_map_size.x - 1));
      p.y = float(Random(0, g_wind_map_size.y - 1));
    }
    float dp = Dot(good_n, (*g_wind_map)[Si32(p.x) + Si32(p.y) * g_wind_map_size.x]);
    if (dp > 0 && Random(0, 100) * 0.01f < dp) {
      is_ok = true;
    }
  }
}

void EasyMain() {
  g_sky.Load("data/sky.tga");
  g_island_1.Load("data/island_1.tga");
  g_island_2.Load("data/island_2.tga");
  ResizeScreen(1920,1080);
  g_font.Load("data/arctic_one_bmf.fnt");
  Clear();
  g_font.Draw(u8"Генерирую острова...", 900,100,
      kTextOriginBottom,kDrawBlendingModeColorize, kFilterNearest, Rgba(255, 128, 128));
  ShowFrame();


  for (Si32 map_idx = 0; map_idx < 2; ++map_idx) {
    g_wind_map = &g_wind_maps[map_idx];
    g_wind_map->resize(g_wind_map_size.x * g_wind_map_size.y);
    for (Si32 n = 0; n < 100; ++n) {
      float r = (float)Random(10, 300);
      Transform2F transform;
      Si64 rot_dir = Random(0, 1);
      transform.dc = DualComplexF(0.f, 0.f, float(rot_dir ? M_PI_2 : -M_PI_2));
      Vec2F center((float)Random((Si64) - r, g_wind_map_size.x - 1 + (Si64)r), (float)Random((Si64)-r, g_wind_map_size.y - 1 + (Si64)r));
      *Log() << "n: " << n << " r: " << r << " center: " << center.x << "," << center.y << " rot_dir: " << rot_dir;
      for (Si32 y = 0; y < g_wind_map_size.y; ++y) {
        for (Si32 x = 0; x < g_wind_map_size.x; ++x) {
          Vec2F from_center = Vec2F((float)x, (float)y) - center;
          Vec2F norm_from_center = Normalize(from_center);
          Vec2F dir = transform.Transform(norm_from_center);
          if (LengthSquared(from_center) >= r*r) {
            dir /= (Length(from_center)-(r-1.f));
          } else if (r > 1){
            dir *= Length(from_center)/sqrt(r);
          }
          (*g_wind_map)[x + y * g_wind_map_size.x] += dir * 500.f / r;
        }
      }
    }
    for (Si32 y = 0; y < g_wind_map_size.y; ++y) {
      for (Si32 x = 0; x < g_wind_map_size.x; ++x) {
        (*g_wind_map)[x + y * g_wind_map_size.x].x += sinf(float(y) * 2.f * (float)M_PI / (float)g_wind_map_size.y) * 50.f;
      }
    }
  }
  g_wind_maps[2].resize(g_wind_map_size.x * g_wind_map_size.y);
  for (Si32 y = 0; y < g_wind_map_size.y; ++y) {
    for (Si32 x = 0; x < g_wind_map_size.x; ++x) {
      g_wind_maps[2][x + y * g_wind_map_size.x] = g_wind_maps[0][x + y * g_wind_map_size.x];
    }
  }
  g_wind_map = &g_wind_maps[2];

  g_dust.reserve(10000);
  for (Si32 idx = 0; idx < 7000; ++idx) {
    g_dust.emplace_back(Vec2F((float)Random(0, g_wind_map_size.x-1), (float)Random(0, g_wind_map_size.y-1)));
  }



  double prev_time = Time();
  double cur_time = Time();
  while (!IsKeyDownward(kKeyEscape)) {
    prev_time = cur_time;
    cur_time = Time();
    double dt = cur_time - prev_time;

    if (IsKeyDown(kKeyUp) || IsKeyDown(kKeyW)) {
      g_player_volume += 1.f * (float)dt;
    }
    if (IsKeyDown(kKeyDown) || IsKeyDown(kKeyS)) {
      g_player_volume -= 1.f * (float)dt;
    }


    if (IsKeyDown(kKey1) || Random(0, 10) == 0) {
      for (Si32 y = 0; y < g_wind_map_size.y; ++y) {
        for (Si32 x = 0; x < g_wind_map_size.x; ++x) {
          g_wind_maps[2][x + y * g_wind_map_size.x] =
            g_wind_maps[2][x + y * g_wind_map_size.x] * (1.0f - 0.5f * (float)dt) +
            g_wind_maps[0][x + y * g_wind_map_size.x] * 0.5f * (float)dt;
        }
      }
    }
    if (IsKeyDown(kKey2)|| Random(0, 10) == 0) {
      for (Si32 y = 0; y < g_wind_map_size.y; ++y) {
        for (Si32 x = 0; x < g_wind_map_size.x; ++x) {
          g_wind_maps[2][x + y * g_wind_map_size.x] =
            g_wind_maps[2][x + y * g_wind_map_size.x] * (1.0f - 0.5f * (float)dt) +
            g_wind_maps[1][x + y * g_wind_map_size.x] * 0.5f * (float)dt;
        }
      }
    }
      

    g_player_volume = Clamp(g_player_volume, 0.f, 1.f);
    g_player_acc.x = 0.f;
    g_player_acc.y = -9.81f + 20.f*g_player_volume;

    Vec2Si32 gpp(g_player_pos);
    Vec2F wind = (*g_wind_map)[Si32(g_player_pos.x) + Si32(g_player_pos.y) * g_wind_map_size.x];
    Check(!isnan(wind.x), "wind.x is nan");
    Check(!isnan(wind.y), "wind.y is nan");

    Vec2F wind_diff = wind - g_player_vel;
    g_player_acc += wind_diff * 0.1f;

    g_player_pos += g_player_vel * (float)dt + g_player_acc * (float)dt * (float)dt * 0.5f;
    g_player_vel += g_player_acc * (float)dt;

    if (g_player_pos.x < 0) {
      g_player_pos.x = 0;
      g_player_vel.x *= -1;
    }
    if (g_player_pos.x >= g_wind_map_size.x) {
      g_player_pos.x = float(g_wind_map_size.x-1);
      g_player_vel.x *= -1;
    }
    if (g_player_pos.y < 0) {
      g_player_pos.y = 0;
      g_player_vel.y *= -1;
    }
    if (g_player_pos.y >= g_wind_map_size.y) {
      g_player_pos.y = (float)(g_wind_map_size.y - 1);
      g_player_vel.y *= -1;
    }

    g_view_pos = g_player_pos - Vec2F(ScreenSize()/2);

    for (Si32 i = (Si32)g_dust.size() - 1; i >= 0; --i) {
      Vec2F &p = g_dust[i];
      p += (*g_wind_map)[Si32(p.x) + Si32(p.y) * g_wind_map_size.x] * (float)dt * (float)Random(1, 5) * 0.2f;
      if (p.x < 0 || p.x >= g_wind_map_size.x ||
          p.y < 0 || p.y >= g_wind_map_size.y) {
        DustRespawn(p);
      }
    }
    for (Si32 i = 0; i < 1; ++i) {
      Si64 idx = Random(0, g_dust.size() - 1);
      g_dust[idx].x = (float)Random(0, g_wind_map_size.x - 1);
      g_dust[idx].y = (float)Random(0, g_wind_map_size.y - 1);
    }

    Clear();
    g_sky.Draw(Vec2Si32(g_view_pos*(-0.5f)-0.3f-Vec2F(ScreenSize())*0.5 + 0.5), g_sky.Size()*2);
    g_island_1.Draw(Vec2Si32(Vec2F(100.f,100.f)-g_view_pos+0.5));
    g_island_2.Draw(Vec2Si32(Vec2F(1300.f, 500.f)-g_view_pos+0.5));

    Vec2Si32 pos = Vec2Si32(g_player_pos - g_view_pos);
    DrawCircle(pos, 12 + Si32(10 * g_player_volume), Rgba(128, 255, 128));

    Rgba sakura_color0(252,220,240);
    Rgba sakura_color1(214,170,189);
    Sprite backbuffer = GetEngine()->GetBackbuffer();
    const Ui32 width_1 = backbuffer.Width()-1;
    const Ui32 height_1 = backbuffer.Height()-1;
    for (Si32 i = (Si32)g_dust.size() - 1; i >= 0; --i) {
      Vec2F &p = g_dust[i];
      Vec2Si32 pos = Vec2Si32(p - g_view_pos);
      pos.x = Clamp(pos.x, 0, width_1);
      pos.y = Clamp(pos.y, 0, height_1);
      Rgba sakura_color = Random(0, 1) ? sakura_color0 : sakura_color1;
      if (backbuffer.RgbaData()[backbuffer.StridePixels()*pos.y + pos.x].g > 120) {
        sakura_color = Rgba(137,85, 95);
      }
      SetPixel(pos.x, pos.y, sakura_color);
      switch (Random(0, 3)) {
        case 1:
          SetPixel(pos.x+1, pos.y, sakura_color);
          break;
        case 2:
          SetPixel(pos.x+1, pos.y+1, sakura_color);
          break;
        case 3:
          SetPixel(pos.x+1, pos.y-1, sakura_color);
          break;
      }
    }

    for (Si32 y = 0; y < g_wind_map_size.y; y += 10) {
      for (Si32 x = 0; x < g_wind_map_size.x; x += 10) {
      //  DrawLine(Vec2Si32(x, y), Vec2Si32(x, y) + Vec2Si32((*g_wind_map)[x + y * g_wind_map_size.x]), Rgba(128, 255, 128));
      }
    }

    ShowFrame();
  }
}
